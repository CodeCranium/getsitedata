﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;

namespace GetIt
{
    class WebSiteInteraction : IDataPuller
    {
        public string EndPointUrl { get; set; }
        
        private string _parentnodename;
        public string ParentNodeName
        {
            get { return _parentnodename; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    const string defaultParentNode = "span";
                    value = defaultParentNode;
                }
                else
                {
                    _parentnodename = value;
                }
            }
        }

        private string _outterhtmlattribue;
        public string OutterHtmlAttribute {
            get { return _outterhtmlattribue; }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                {
                    const string defaultOutterHtmlAttrib = "hrdlnk";
                    value = defaultOutterHtmlAttrib;
                }
                else
                {
                    _outterhtmlattribue = value;
                }
            }
        }

        private static int _amountOfTimesRun = 0;

        public WebSiteInteraction(string parentnodename, string outterhtmlattribue)
        {
            this._parentnodename = parentnodename;
            this._outterhtmlattribue = outterhtmlattribue;
        }

        public void CheckSite(ref IList<string> currentItems)
        {
            var docInfo = new HtmlWeb();
            var docuemnt123 = docInfo.Load(EndPointUrl);
            //Mobile site (rendered easier since it is in list format not Picture format.
            var aTags = docuemnt123.DocumentNode.SelectNodes(@"//a");

            if (aTags != null)
            {
                foreach (var aTag in aTags)
                {
                    if ((aTag.ParentNode.Name == ParentNodeName) && (aTag.OuterHtml.Contains(OutterHtmlAttribute)))
                    {
                        var itemDescription = aTag.InnerHtml;
                        var itemLink = aTag.Attributes["href"].Value;
                        if (_amountOfTimesRun >= 1)
                        {
                            var checkItem = currentItems.FirstOrDefault(x => x.Contains(itemDescription));
                            if (checkItem == null)
                            {
                                //New Items NOT found on the list already.

                                var newItemDescription = itemDescription;
                                var newItemLink = itemLink;

                                Alert sendNotice = new Alert()
                                {
                                    NewItemDescription = newItemDescription,
                                    NewItemLink = newItemLink
                                };
                                sendNotice.SendAlert();

                                //Add item to list, after processing it as it is no longer new.
                                currentItems.Add(newItemDescription);
                            }
                        }

                        else
                        {
                            // Add all items (Referencable Description) to list (since it is first time to run)
                            Logger.LogEntry(Logger.LogType.Info, String.Format("Loading inital data.  [{0}]", itemDescription));
                            currentItems.Add(itemDescription);
                        }
                    }
                }
                _amountOfTimesRun += 1;
            }
        }
    }
}
