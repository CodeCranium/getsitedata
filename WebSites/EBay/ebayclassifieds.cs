﻿using System;
using System.Collections.Generic;

namespace GetIt
{
    class ebayclassifieds : IVendor
    {

        public void CheckForNew(ref IList<string> currentItems, string city, string searchParam)
        {
            try
            {
                WebSiteInteraction checkSite = new WebSiteInteraction(parentnodename: "p", outterhtmlattribue: "class=\"t\"");
                checkSite.EndPointUrl = String.Format("http://{0}.ebayclassifieds.com/classified-ads/?q={1}", city, searchParam);
                checkSite.CheckSite(ref currentItems);
            }
            catch (Exception ex)
            {
                Logger.LogEntry(Logger.LogType.Error, ex.Message);
            }
        }
    }

    class ebay : ebayclassifieds
    {
        // An easy way to allow the instanciation of the object to use the name 'ebay' instead of having to fully state 'ebayclassifieds'
        // this instance name 'ebay' will inherit and run the same as 'ebayclassifieds'
    }


    //EBayClassifieds ebSearch = new EBayClassifieds();
    //BaseUrl = @"http://sanantonio.ebayclassifieds.com";
    //SearchParam = "iphone";
    //CheckForNew(ref currentItems);
}
