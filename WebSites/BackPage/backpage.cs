﻿using System;
using System.Collections.Generic;

namespace GetIt
{
    class backpage : IVendor
    {
        public void CheckForNew(ref IList<string> currentItems, string city, string searchParam)
        {
            try
            {
                WebSiteInteraction checkSite = new WebSiteInteraction(parentnodename: "span", outterhtmlattribue: String.Empty);
                checkSite.EndPointUrl = String.Format("http://{0}.backpage.com/buyselltrade/?keyword={1}", city, searchParam);
                checkSite.CheckSite(ref currentItems);
            }
            catch (Exception ex)
            {
                Logger.LogEntry(Logger.LogType.Error, ex.Message);
            }
        }
    }

    //BackPage bpSearch = new BackPage();
    //BaseUrl = "http://sanantonio.backpage.com";
    //SearchParam = "iphone";
    //CheckForNew(ref currentItems);
}
