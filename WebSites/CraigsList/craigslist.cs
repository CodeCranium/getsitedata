﻿using System;
using System.Collections.Generic;

namespace GetIt
{
    class craigslist : IVendor
    {
        public void CheckForNew(ref IList<string> currentItems, string city, string searchParam)
        {
            try
            {
                WebSiteInteraction checkSite = new WebSiteInteraction(parentnodename: "span", outterhtmlattribue: "hdrlnk")
                                                   {
                                                       EndPointUrl = String.Format("http://{0}.craigslist.org/search/sss/?sort=date&query={1}", city, searchParam)
                                                   };
                checkSite.CheckSite(ref currentItems);
            }
            catch (Exception ex)
            {
                Logger.LogEntry(Logger.LogType.Error, ex.Message);
            }
        }
    }

    /*   Command Line Example:  GetIt.exe type=craigslist city="San Antonio" search=iphone   */ 
}
