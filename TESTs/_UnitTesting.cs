﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using NUnit.Framework;

namespace GetIt
{
    class _UnitTesting
    {

        private IList<string> _currentItems = new List<string>();

        [Test]
        public void CollectResultsForTestingEBaySite()
        {
            // Example endpoint:  http://sanantonio.ebayclassifieds.com/classified-ads/?q=iphone

            string baseUrl = "http://sanantonio.ebayclassifieds.com";
            string searchParam = "iphone";
            WebSiteInteraction checkSite = new WebSiteInteraction(parentnodename: "p", outterhtmlattribue: "class=\"t\""); 
            checkSite.EndPointUrl = String.Format("{0}/classified-ads/?q={1}", baseUrl, searchParam);
            checkSite.CheckSite(ref _currentItems);

            Assert.That(_currentItems.Count != 0);
        }

        [Test]
        public void CollectUsingEBayShortenClass()
        {
            string baseUrl = "http://sanantonio.ebayclassifieds.com";
            string searchFor = "iphone";

            ebay x1 = new ebay();
            x1.CheckForNew(ref _currentItems, baseUrl, searchFor);

            Assert.That(_currentItems.Count != 0);
        }

        [Test]
        public void CollectResultsForCraigslist()
        {
            // Example endpoint:  http://sanantonio.craigslist.org/search/sss?query=iphone&sort=rel

            string baseUrl = "http://sanantonio.craigslist.org";
            string searchParam = "iphone";
            WebSiteInteraction checkSite = new WebSiteInteraction(parentnodename: "span", outterhtmlattribue: "hdrlnk");
            checkSite.EndPointUrl = String.Format("{0}/search/sss?query={1}", baseUrl, searchParam);
            checkSite.CheckSite(ref _currentItems);

            Assert.That(_currentItems.Count != 0);
        }
    }
}
