﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace GetIt
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            try
            {
                IList<string> currentItems = new List<string>();
                DateTime endSearch = DateTime.Now.AddMinutes(120);

                string typename = null, city = null, searchfor = null;

                foreach (string argItem in args)
                {
                    string[] argInfo = argItem.Split('=');

                    switch (argInfo[0])
                    {
                        case "type":
                            typename = argInfo[1];
                            break;
                        case "city":
                            city = argInfo[1];
                            break;
                        case "search":
                            searchfor = argInfo[1];
                            break;
                    }
                }

                if ((typename == null) || (city == null) || (searchfor == null))
                {
                    Debug.WriteLine("(Required) typename: " + typename);
                    Debug.WriteLine("(Required) city: " + city);
                    Debug.WriteLine("(Required) searchfor: " + searchfor);
                    throw new Exception("Incorrect syntax provided, please ensure that all required paramters are provided.");
                }

                while (endSearch > DateTime.Now)
                {
                    currentItems = CheckForNewInstances(typename, city, searchfor, currentItems);
                    Thread.Sleep(15000); // Sleep for 15 seconds.
                }


                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.LogEntry(Logger.LogType.Critical, ex.Message);
                throw;
            }
        }

        private static IList<string> CheckForNewInstances(string typename, string city, string searchfor, IList<string> currentItems)
        {
            // Generic Specified
            if (typename != null)
            {
                Type vendorType = Type.GetType("GetIt." + typename.ToLower());
                if (vendorType != null)
                {
                    var searcher = (IVendor) Activator.CreateInstance(vendorType);
                    searcher.CheckForNew(ref currentItems, city.Replace(" ", String.Empty).Trim(), searchfor);
                }
            }
            Logger.LogEntry(Logger.LogType.Info,
                            String.Format("Processed: {0}, Number of items in current collection: {1}", typename, currentItems.Count));
            return currentItems;
        }
    }
}

// GetIt.exe type=craigslist city="Denver" search=iphone