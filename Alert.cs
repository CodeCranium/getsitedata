﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GetIt
{
    class Alert
    {
        public string NewItemDescription { get; set; }
        public string NewItemLink { get; set; }

        internal void SendAlert()
        {
            Logger.LogEntry(Logger.LogType.NewItemFound, NewItemDescription);
        }
    }
}
