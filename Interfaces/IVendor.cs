﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GetIt
{
    interface IVendor
    {
        void CheckForNew(ref IList<string> currentItems, string city, string searchParam);
    }
}
