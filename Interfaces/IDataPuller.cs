﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GetIt
{
    interface IDataPuller
    {
        string EndPointUrl { get; set; }

        void CheckSite(ref IList<string> currentItems);
    }
}
