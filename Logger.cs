﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace GetIt
{
    public abstract class Logger
    {
        public enum LogType
        {
            Info = 0,
            Warning = 1,
            Error = 2,
            Critical = 3,
            NewItemFound = 4,
        }
        public static void LogEntry(LogType logLevel, string messageInfo)
        {
            var messageData = String.Format("[{0}] {1} : {2}", logLevel, DateTime.Now, messageInfo);
            
            using (var xLog = new StreamWriter(@"LOG.txt", true))
            {
                Debugger.Log(0, "Logging Info", messageData);
                xLog.WriteLine(messageData);
            }

        }
    }
}
